﻿# The script of the game goes in this file.

# Declare characters used by this game. The color argument colorizes the
# name of the character.

define e = Character("Johnny")
define b = Character("Bruno")
define n = Character("Ernesto")
image bg work = "office_background.jpg"
image bg home = "home_background.jpg"
# The game starts here.

screen Day:
     text "Day [persistent.dayCounter]" xalign 1.0 ypos 30
     text "$ [persistent.cash]" xalign 1.0 ypos 75


label start:
    show screen Day
    # Show a background. This uses a placeholder by default, but you can
    # add a file (named either "bg room.png" or "bg room.jpg") to the
    # images directory to show it.

    if persistent.dayCounter  == 1:
        scene bg test
    else:
        scene bg room

    # This shows a character sprite. A placeholder is used, but you can
    # replace it by adding a file named "eileen happy.png" to the images
    # directory.
    if persistent.dayCounter  == 1:
        show pikachu_example
    else:
        show bruno happy

    # These display lines of dialogue.

    e "You've created a new Ren'Py game."

    e "Once you add a story, pictures, and music, you can release it to the world!"

label c1:
    if persistent.dayCounter == 30:
        e "DAY 30 - NEW YEARS - THE END!!!!"
        return

    menu:
        "Work":
            jump Work

        "Family":
            jump Family

        "Shopping":
            jump Shopping

label Work:
    show bg work
    b "Welcome to work, I'm Bruno your boss"
    b "I'm a badass boss, let me know if you need anything"
    # Add place to make money here
    $ persistent.cash +=100
    $ persistent.dayCounter += 1
    jump c1

label Family:
    show bg home
    n "Welcome home family member"
    $ persistent.dayCounter += 1
    jump c1

label Shopping:
    show bg shopping_background
    n "We're shopping at the store"
    $ persistent.dayCounter += 1
    jump c1


    # This ends the game.
